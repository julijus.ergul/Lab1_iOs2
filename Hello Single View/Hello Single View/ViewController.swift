//
//  ViewController.swift
//  Hello Single View
//
//  Created by Julijus Ergul on 2017-10-16.
//  Copyright © 2017 Julijus Ergul. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func buttonTap(_ sender: UIButton) {
        print("Hello button")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

